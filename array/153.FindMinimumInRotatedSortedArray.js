/*
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).

Find the minimum element.

You may assume no duplicate exists in the array.

Example 1:

Input: [3,4,5,1,2] 
Output: 1
Example 2:

Input: [4,5,6,7,0,1,2]
Output: 0
*/

// https://leetcode.com/articles/find-minimum-in-rotated-sorted-array/

var findMin = function(nums) {
  var left = 0;
  var right = nums.length - 1;

  while (left < right) {
    var mid = left + Math.floor((right - left) / 2);

    if (nums[mid] < nums[right]) {
      right = mid;
    } else {
      left = mid + 1;
    }
  }

  return nums[left];
};

/*
Time Complexity : Same as Binary Search O(logN)
Space Complexity : O(1)
*/

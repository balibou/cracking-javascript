/*
Given an array of 2n integers, your task is to group these integers into n pairs of integer, say (a1, b1), (a2, b2), ..., (an, bn) which makes sum of min(ai, bi) for all i from 1 to n as large as possible.

Example 1:
Input: [1,4,3,2]

Output: 4
Explanation: n is 2, and the maximum sum of pairs is 4 = min(1, 2) + min(3, 4).
Note:
n is a positive integer, which is in the range of [1, 10000].
All the integers in the array will be in the range of [-10000, 10000].
*/

/**
 * @param {number[]} nums
 * @return {number}
 */
var arrayPairSum = function(nums) {
  quicksort(nums, 0, nums.length - 1);
  var sum = 0;
  for (var i = 0; i < nums.length; i++) {
    if (i % 2 === 0) sum += Math.min(nums[i], nums[i + 1]);
  }
  return sum;
};

function quicksort(arr, start, end) {
  if (start < end) {
    var pi = partition(arr, start, end);
    quicksort(arr, start, pi - 1);
    quicksort(arr, pi + 1, end);
  }
}

function partition(arr, low, high) {
  var pivot = arr[high];
  var i = low - 1;
  for (var j = low; j < high; j++) {
    if (arr[j] <= pivot) {
      i++;

      swap(arr, i, j);
    }
  }

  swap(arr, i + 1, high);

  return i + 1;
}

function swap(arr, i, j) {
  var temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

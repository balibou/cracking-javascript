/*
Given a non-empty array of integers, every element appears twice except for one. Find that single one.

Note:

Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?

Example 1:

Input: [2,2,1]
Output: 1
Example 2:

Input: [4,1,2,1,2]
Output: 4
*/

// Approach 2: Hash Table
// Time complexity: O(n)
// Space complexity: O(n)

/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNumber = function(nums) {
  var hash = {};
  for (var i = 0; i < nums.length; i++) {
    if (hash[nums[i]] === undefined) hash[nums[i]] = 0;
    hash[nums[i]] += 1;
  }

  return Object.keys(hash).filter(e => {
    if (hash[e] === 1) return e;
  });
};

/*
Approach 4: Bit Manipulation
Time complexity: O(n)
Space complexity: O(1)
Concept

If we take XOR of zero and some bit, it will return that bit
a ⊕ 0 = a
If we take XOR of two same bits, it will return 0
a ⊕ a = 0
a ⊕ b ⊕ a = (a ⊕ a) ⊕ b = 0 ⊕ b = b
So we can XOR all bits together to find the unique number.
*/

var singleNumber = function(nums) {
  var num = 0;
  for (var i = 0; i < nums.length; i++) {
    num ^= nums[i];
  }

  return num;
};

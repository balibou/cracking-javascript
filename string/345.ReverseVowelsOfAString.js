/*
Write a function that takes a string as input and reverse only the vowels of a string.

Example 1:

Input: "hello"
Output: "holle"
Example 2:

Input: "leetcode"
Output: "leotcede"
Note:
The vowels does not include the letter "y".
*/

/**
 * @param {string} s
 * @return {string}
 */
var reverseVowels = function(s) {
  let left = 0;
  let right = s.length - 1;
  let vowels = 'aeiouAEIOU';
  s = s.split('');
  while (left < right) {
    if (vowels.includes(s[left]) && vowels.includes(s[right])) {
      let temp = s[left];
      s[left] = s[right];
      s[right] = temp;
      left++;
      right--;
    }
    if (!vowels.includes(s[left])) {
      left++;
    }
    if (!vowels.includes(s[right])) {
      right--;
    }
  }
  return s.join('');
};

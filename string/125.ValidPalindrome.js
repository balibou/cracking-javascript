/*

Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

Note: For the purpose of this problem, we define empty string as valid palindrome.

Example 1:

Input: "A man, a plan, a canal: Panama"
Output: true
Example 2:

Input: "race a car"
Output: false
*/

/**
 * @param {string} s
 * @return {boolean}
 */
var isPalindrome = function(s) {
  var modifiedString = s.replace(/[^\w]/gi, '');
  return helper(0, modifiedString.length - 1, modifiedString);
};

function helper(first, last, s) {
  if (first >= last) return true;
  if (s[first].toLowerCase() !== s[last].toLowerCase()) return false;

  return helper(first + 1, last - 1, s);
}

/*
Given a binary tree, return the preorder traversal of its nodes' values.

Example:

Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,2,3]

Follow up: Recursive solution is trivial, could you do it iteratively?
*/

var preorderTraversal = function(root) {
  let result = [];
  recurse(root);
  return result;

  function recurse(node) {
    if (node == null) {
      return;
    }
    result.push(node.val);
    if (node.left) recurse(node.left);
    if (node.right) recurse(node.right);
  }
};

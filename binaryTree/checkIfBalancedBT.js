var BT = require('./utils');

// #1

/*
1
 \
  2
   \
    3
     \
      4
*/

/*
     1 (-1+1+1+1+1 = 3)
    / \
  null 2 (-1+1+1+1 = 2)
  (-1)  \
         3 (-1+1+1 = 1)
          \
           4 (-1+1 = 0)
            \
             null (-1)
*/

function getHeight(root) {
  // Check if node is null
  if (root == null) return -1;
  // Recurse on left and right nodes to calculate maximum heigth of node
  return Math.max(getHeight(root.left), getHeight(root.right)) + 1;
}

function isBalanced(root) {
  // Check if node is null
  if (root == null) return true;
  // Get difference of height between left and right nodes
  var heightDiff = getHeight(root.left) - getHeight(root.right);
  if (Math.abs(heightDiff) > 1) {
    // If absolute difference > 1 BT is unbalanced
    return false;
  } else {
    // Recurse on left and right nodes
    return isBalanced(root.left) && isBalanced(root.right);
  }
}

console.log(isBalanced(BT.bt2a));
console.log(isBalanced(BT.bt3a));

// #2

function checkHeight(root) {
  // Check if node is null
  if (root == null) return -1;
  // Calculate left heigth
  var leftHeight = checkHeight(root.left);
  // Pass up error
  if (leftHeight == Number.MIN_VALUE) return Number.MIN_VALUE;
  // Calculate right heigth
  var rightHeight = checkHeight(root.right);
  // Pass error up
  if (rightHeight == Number.MIN_VALUE) return Number.MIN_VALUE;
  // Calculate difference of height between left and right nodes
  var heightDiff = leftHeight - rightHeight;
  // If absolute difference > 1 BT is unbalanced
  if (Math.abs(heightDiff) > 1) {
    return Number.MIN_VALUE;
    // Else calculate max height of the node
  } else {
    return Math.max(leftHeight, rightHeight) + 1;
  }
}

function isBalanced2(root) {
  return checkHeight(root) != Number.MIN_VALUE;
}

console.log(isBalanced2(BT.bt2a));
console.log(isBalanced2(BT.bt3a));

// #3 Best Leetcode

function isBalanced3(root) {
  var flag = true;
  function checkTree(node) {
    if (!flag) return;
    if (node) {
      var left = checkTree(node.left);
      var right = checkTree(node.right);
      if (Math.abs(left - right) > 1) {
        flag = false;
        return;
      }
      return Math.max(left, right) + 1;
    }
    return 0;
  }
  checkTree(root);
  return flag;
}

console.log(isBalanced3(BT.bt2a));
console.log(isBalanced3(BT.bt3a));

/*
Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left to right, then right to left for the next level and alternate between).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7
return its zigzag level order traversal as:
[
  [3],
  [20,9],
  [15,7]
]
*/

var zigzagLevelOrder = function(root) {
  var res = [];
  traverse(root, 0);
  return res;

  function traverse(node, i) {
    if (!node) return;
    if (res[i] === undefined) res[i] = [];
    i % 2 === 0 ? res[i].push(node.val) : res[i].unshift(node.val);
    if (node.left) traverse(node.left, i + 1);
    if (node.right) traverse(node.right, i + 1);
  }
};

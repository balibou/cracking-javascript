/*
Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7
return its bottom-up level order traversal as:
[
  [15,7],
  [9,20],
  [3]
]

Definition for a binary tree node.

function TreeNode(val) {
  this.val = val;
  this.left = this.right = null;
}
*/

var levelOrderBottom = function(root) {
  var res = [];
  levelRecursion(root, res, 0);
  return res.reverse();
};

function levelRecursion(node, res, level) {
  if (node === null) return;

  if (res.length < level + 1) {
    res.push([]);
  }

  levelRecursion(node.left, res, level + 1);
  levelRecursion(node.right, res, level + 1);

  res[level].push(node.val);
}

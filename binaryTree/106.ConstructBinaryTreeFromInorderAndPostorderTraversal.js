/*
Given inorder and postorder traversal of a tree, construct the binary tree.

Note:
You may assume that duplicates do not exist in the tree.

For example, given

inorder = [9,3,15,20,7]
postorder = [9,15,7,20,3]
Return the following binary tree:

    3
   / \
  9  20
    /  \
   15   7
*/

var buildTree = function(inorder, postorder) {
  function build(l, r) {
    if (l > r) {
      return null;
    }
    let val = postorder.pop();
    let i = inorder.indexOf(val);
    let root = new TreeNode(val);

    root.right = build(i + 1, r);
    root.left = build(l, i - 1);

    return root;
  }
  return build(0, inorder.length - 1);
};

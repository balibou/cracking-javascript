var BinaryTree = function(value) {
  this.value = value;
  this.left = null;
  this.right = null;
};

var bt1a = new BinaryTree(6);
var bt1b = new BinaryTree(4);
var bt1c = new BinaryTree(7);
var bt1d = new BinaryTree(1);
var bt1e = new BinaryTree(5);

bt1a.left = bt1b;
bt1a.right = bt1c;
bt1b.left = bt1d;
bt1b.right = bt1e;

/*
6
|\
4 7
|\
1 5
*/

var bt2a = new BinaryTree(6);
var bt2b = new BinaryTree(4);
var bt2c = new BinaryTree(7);
var bt2d = new BinaryTree(1);
var bt2e = new BinaryTree(3);

bt2a.left = bt2b;
bt2a.right = bt2c;
bt2b.left = bt2d;
bt2b.right = bt2e;

/*
6
|\
4 7
|\
1 3
*/

var bt3a = new BinaryTree(1);
var bt3b = new BinaryTree(2);
var bt3c = new BinaryTree(3);
var bt3d = new BinaryTree(4);

bt3a.right = bt3b;
bt3b.right = bt3c;
bt3c.right = bt3d;

/*
1
 \
  2
   \
    3
     \
      4
*/

var BT = { bt1a, bt2a, bt3a };
module.exports = BT;

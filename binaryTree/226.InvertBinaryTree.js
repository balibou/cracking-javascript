/*Invert a binary tree.

Example:

Input:

     4
   /   \
  2     7
 / \   / \
1   3 6   9
Output:

     4
   /   \
  7     2
 / \   / \
9   6 3   1
*/

var invertTree = function(root) {
  if (root == null) return null;
  var left = invertTree(root.left);
  var right = invertTree(root.right);
  root.left = right;
  root.right = left;
  return root;
};

/*
This is a classic tree problem that is best-suited for a recursive approach.

Algorithm

The inverse of an empty tree is the empty tree.
The inverse of a tree with root rr, and subtrees \mbox{right} and \mbox{left}, is a tree with root rr, whose right subtree is the inverse of \mbox{left}, and whose left subtree is the inverse of \mbox{right}.

Complexity Analysis

Time complexity: O(n).
Space complexity: O(n).

*/

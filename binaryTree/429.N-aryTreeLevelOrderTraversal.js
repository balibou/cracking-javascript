/*
Given an n-ary tree, return the level order
traversal of its nodes' values. (ie, from left to right, level by level).

For example, given a 3-ary tree:

      1
   /  |  \
  3   2   4
 / \
5   6

We should return its level order traversal:

[
  [1],
  [3,2,4],
  [5,6]
]

Definition for a Node.

function Node(val,children) {
  this.val = val;
  this.children = children;
};

*/

function traverse(node, container, current_depth) {
  if (node.children) {
    for (var i = 0; i < node.children.length; i++) {
      if (i === 0 && !container[current_depth]) {
        container.push([]);
      }
      traverse(node.children[i], container, current_depth + 1);
    }
    for (var j = 0; j < node.children.length; j++) {
      container[current_depth].push(node.children[j].val);
    }
  }
}

var levelOrder = function levelOrder(root) {
  if (!root) return [];

  var container = [[root.val]];
  traverse(root, container, 1);
  return container;
};

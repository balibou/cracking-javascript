/*

Balanced trees

For any node in AVL, the height of its left subtree differs by at most 1 from the height of its right subtree.
The "height" of a node in a binary tree is the length of the longest path from that node to a leaf.
People define the height of an empty tree to be (-1).

Unbalanced trees:

              A  (Height = 2)
           /     \
(height =-1)       B (Height = 1) <-- Unbalanced because 1-(-1)=2 >1
                    \
                     C (Height = 0)


        A (h=3)
     /     \
 B(h=0)     C (h=2)        <-- Unbalanced: 2-0 =2 > 1
           /   \
        E(h=1)  F (h=0)
        /     \
      H (h=0)   G (h=0) 


Balanced tree:

        A (h=3)
     /     \
 B(h=1)     C (h=2)        
/          /   \
D (h=0)  E(h=0)  F (h=1)
               /
              G (h=0)

*/

/*
Given a binary tree, return the inorder traversal of its nodes' values.

Example:

Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,3,2]
Follow up: Recursive solution is trivial, could you do it iteratively?
*/

var inorderTraversal = function(root) {
  var final = [];
  traverse(root);
  return final;

  function traverse(node) {
    if (!node) return;
    if (node.left) traverse(node.left);
    final.push(node.val);
    if (node.right) traverse(node.right);
  }
};

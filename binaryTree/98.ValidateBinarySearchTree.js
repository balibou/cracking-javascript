var BT = require('./utils');

/*
Given a binary tree, determine if it is a valid binary search tree (BST).

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than the node's key.
The right subtree of a node contains only nodes with keys greater than the node's key.
Both the left and right subtrees must also be binary search trees.
Example 1:

Input:
    2
   / \
  1   3
Output: true
Example 2:

    5
   / \
  1   4
     / \
    3   6
Output: false
Explanation: The input is: [5,1,4,null,null,3,6]. The root node's value
is 5 but its right child's value is 4.
*/

// #1 In-Order Traversal (working only with uniq values)

var last_printed = null;

function checkIfBST1(node) {
  // Check if node is null
  if (node == null) return true;
  // Check / recurse left
  if (!checkIfBST1(node.left)) return false;
  // Check current
  if (last_printed != null && node.value <= last_printed) return false;
  // Current value is now last printed
  last_printed = node.value;
  // Check / recurse right
  if (!checkIfBST1(node.right)) return false;
  // All good!
  return true;
}

console.log('bt1a', checkIfBST1(BT.bt1a));
console.log('bt2a', checkIfBST1(BT.bt2a));

// #2 Min / Max value

var isValidBST = function(root, min = null, max = null) {
  if (!root) return true;
  if (
    (min === null || root.value > min) &&
    (max === null || root.value < max)
  ) {
    return (
      isValidBST(root.left, min, root.value) &&
      isValidBST(root.right, root.value, max)
    );
  } else {
    return false;
  }
};

console.log('bt1a', isValidBST(BT.bt1a, null, null));
console.log('bt2a', isValidBST(BT.bt2a, null, null));

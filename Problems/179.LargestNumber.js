/*
Given a list of non negative integers, arrange them such that they form the largest number.

Example 1:

Input: [10,2]
Output: "210"
Example 2:

Input: [3,30,34,5,9]
Output: "9534330"
Note: The result may be very large, so you need to return a string instead of an integer.
*/

/**
 * @param {number[]} nums
 * @return {string}
 */
var largestNumber = function(nums) {
  let strNums = [];

  // turn each number to string
  nums.forEach(e => strNums.push(`${e}`));

  // sort by comparing sum of a + b and b + a
  strNums.sort((a, b) => {
    a = a + b;
    b = b + a;
    if (a > b) return -1;
    else if (a < b) return 1;
    return 0;
  });

  strNums = strNums.join('');
  return parseInt(strNums) == 0 ? '0' : strNums;
};

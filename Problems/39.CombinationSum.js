/*
Given a set of candidate numbers (candidates) (without duplicates) and a target number (target), find all unique combinations in candidates where the candidate numbers sums to target.

The same repeated number may be chosen from candidates unlimited number of times.

Note:

All numbers (including target) will be positive integers.
The solution set must not contain duplicate combinations.
Example 1:

Input: candidates = [2,3,6,7], target = 7,
A solution set is:
[
  [7],
  [2,2,3]
]
Example 2:

Input: candidates = [2,3,5], target = 8,
A solution set is:
[
  [2,2,2,2],
  [2,3,3],
  [3,5]
]
*/

/**
 * @param {number[]} candidates
 * @param {number} target
 * @return {number[][]}
 */
function combinationSum(nums, target) {
  var list = [];
  nums.sort();
  backtrack(list, [], nums, target, 0);
  return list;
}

function backtrack(list, tempList, nums, remain, start) {
  if (remain < 0) return;
  else if (remain == 0) list.push([...tempList]);
  else {
    for (var i = start; i < nums.length; i++) {
      tempList.push(nums[i]);
      backtrack(list, tempList, nums, remain - nums[i], i); // not i + 1 because we can reuse same elements
      tempList.pop();
    }
  }
}

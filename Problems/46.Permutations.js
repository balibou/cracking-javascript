/*
Given a collection of distinct integers, return all possible permutations.

Example:

Input: [1,2,3]
Output:
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]
*/

/**
 * @param {number[]} nums
 * @return {number[][]}
 */
function permute(nums) {
  var list = [];
  backtrack(list, [], nums);
  return list;
}

function backtrack(list, tempList, nums) {
  if (tempList.length == nums.length) {
    list.push([...tempList]);
  }
  for (var i = 0; i < nums.length; i++) {
    if (tempList.includes(nums[i])) continue; // element already exists, skip
    tempList.push(nums[i]);
    backtrack(list, tempList, nums);
    tempList.pop();
  }
}

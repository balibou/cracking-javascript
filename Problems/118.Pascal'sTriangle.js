/*
Given a non-negative integer numRows,
generate the first numRows of Pascal's triangle.

Example:

Input: 5
Output:
[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
*/

var generate = function(numRows) {
  var triangle = [];

  // First base case; if user requests zero rows, they get zero rows.
  if (numRows == 0) {
    return triangle;
  }

  // Second base case; first row is always [1].
  triangle.push([]);
  triangle[0].push(1);

  for (var rowNum = 1; rowNum < numRows; rowNum++) {
    var row = [];
    var prevRow = triangle[rowNum - 1];

    // The first row element is always 1.
    row.push(1);

    // Each triangle element (other than the first and last of each row)
    // is equal to the sum of the elements above-and-to-the-left and
    // above-and-to-the-right.
    for (var j = 1; j < rowNum; j++) {
      row.push(prevRow[j - 1] + prevRow[j]);
    }

    // The last row element is always 1.
    row.push(1);

    triangle.push(row);
  }

  return triangle;
};

/*
Given a linked list, remove the n-th node from the end of list and return its head.

Example:

Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list becomes 1->2->3->5.
Note:

Given n will always be valid.

Follow up:

Could you do this in one pass?
*/

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} n
 * @return {ListNode}
 */
var removeNthFromEnd = function(head, n) {
  let nodeToReturn = head;
  let pointer1 = head;
  let pointer2 = head;

  for (let i = 0; i < n; i++) {
    pointer2 = pointer2.next;
  }

  if (!pointer2) {
    return nodeToReturn.next;
  }

  while (pointer2.next) {
    pointer1 = pointer1.next;
    pointer2 = pointer2.next;
  }

  pointer1.next = pointer1.next.next;

  return nodeToReturn;
};

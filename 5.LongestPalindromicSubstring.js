/*
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: "cbbd"
Output: "bb"
*/

var longestPalindrome = function(s) {
  let res = '';
  for (let i = 0; i < s.length; i++) {
    let tmp1 = func(s, i, i);
    let tmp2 = func(s, i, i + 1);
    let tmp = tmp1.length > tmp2.length ? tmp1 : tmp2;
    res = tmp.length > res.length ? tmp : res;
  }
  return res;
};
function func(str, i, j) {
  while (i >= 0 && j <= str.length - 1 && str[i] == str[j]) {
    i--;
    j++;
  }
  return str.slice(i + 1, j);
}

// T: O(n^2)
// S: O(1)

// https://aonecode.com/amazon-online-assessment-questions

const movie_duration = [90, 85, 75, 60, 120, 150, 125];
const d = 250;

function findPairOfMovies(movie_duration, target) {
  var list = [];
  movie_duration.sort();
  backtracking(movie_duration, target, list, [], 0);
  return list[0];
}

function backtracking(movie_duration, target, list, tempList, start) {
  if (target < 0) return;
  if (list.length > 0) {
    if (
      tempList.length === 2 &&
      tempList[0] + tempList[1] > list[0][0] + list[0][1]
    )
      list.unshift([...tempList]);
  } else {
    if (tempList.length === 2) list.unshift([...tempList]);
  }

  for (var i = start; i < movie_duration.length; i++) {
    tempList.push(movie_duration[i]);
    backtracking(
      movie_duration,
      target - movie_duration[i],
      list,
      tempList,
      i + 1
    );
    tempList.pop();
  }
}

var test = findPairOfMovies(movie_duration, d - 30);
console.log(test);

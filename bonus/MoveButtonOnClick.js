// HTML
// <button class="button" id='test'>here</button>

/* CSS
.button.move {
  margin-top:200px;
}
*/

// JS
var div = document.getElementById('test');
div.addEventListener('click', move);

function move() {
  div.className = div.className + ' move';
}

meetings = [
  {
    name: 'Meeting 1',
    hours: 2
  },
  {
    name: 'Meeting 5',
    hours: 1
  },
  {
    name: 'Meeting 2',
    hours: 3
  },
  {
    name: 'Meeting 3',
    hours: 4
  },
  {
    name: 'Meeting 4',
    hours: 5
  }
];

var haveHours = 5;

function optimizeMeetings(meetings, haveHours) {
  var list = [];
  meetings.sort();
  backtrack(meetings, haveHours, list, [], 0);
  return list;
}

function backtrack(meetings, haveHours, list, tempList, start) {
  if (haveHours < 0) return;
  if (haveHours === 0) list.push([...tempList]);
  for (var i = start; i < meetings.length; i++) {
    if (i > start && meetings[i] == meetings[i - 1]) continue;
    tempList.push(meetings[i]);
    backtrack(meetings, haveHours - meetings[i].hours, list, tempList, i + 1);
    tempList.pop();
  }
}

console.log(optimizeMeetings(meetings, haveHours));

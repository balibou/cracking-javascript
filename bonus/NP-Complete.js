// http://xkcd.com/287/

function combinationSum(nums, target) {
  var list = [];
  nums.sort();
  backtrack(list, [], nums, target, 0);
  return list;
}

function backtrack(list, tempList, nums, remain, start) {
  if (remain < 0) return;
  else if (remain == 0) list.push([...tempList]);
  else {
    for (var i = start; i < nums.length; i++) {
      tempList.push(nums[i]);
      var rest = remain - nums[i][Object.keys(nums[i])[0]];
      backtrack(list, tempList, nums, rest.toFixed(2), i);
      tempList.pop();
    }
  }
}

var appetizers = [
  { 'mixed-fruits': 2.15 },
  { 'french-fries': 2.75 },
  { 'side-salad': 3.35 },
  { 'hot-wings': 3.55 },
  { 'mozarella-sticks': 4.2 },
  { 'sampler-plate': 5.8 }
];
var target = 15.05;

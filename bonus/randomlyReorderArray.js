// https://www.youtube.com/watch?v=CoI4S7z1E1Y&list=PLBZBJbE_rGRVnpitdvpdY9952IsKMDuev&index=11

function reorder(num) {
  var arr = num
    .toString(10)
    .split('')
    .map(e => parseInt(e));

  for (var i = arr.length - 1; i >= 0; i--) {
    var pick = Math.floor(Math.random() * i);

    temp = arr[i];
    arr[i] = arr[pick];
    arr[pick] = temp;
  }

  return arr;
}

var num = 10392;
reorder(num); // O(n) O(1)

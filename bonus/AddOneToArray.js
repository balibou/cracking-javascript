// https://www.youtube.com/watch?v=uQdy914JRKQ

function addOne(arr) {
  var carry = 1;
  var res = [];

  for (var i = arr.length - 1; i >= 0; i--) {
    var total = arr[i] + carry;

    if (total === 10) carry = 1;
    else carry = 0;

    res[i] = total % 10;
  }

  if (carry === 1) {
    res = [1, ...res];
  }

  console.log(res);
}

var arr = [9, 9, 9];
addOne(arr); // 0(n) O(n)

// https://www.careercup.com/question?id=5739126414901248

/*
Given
colors = ["red", "blue", "green", "yellow"];
and a string
str = "Lorem ipsum dolor sit amet";
write a function that prints each letter of the string in different colors.
ex:
L is red, o is blue, r is green, e is yellow, m is red, after the space, i should be blue.
*/

var colors = ['red', 'blue', 'green', 'yellow'];
var str = 'Lorem ipsum dolor sit amet';

printLetterWithColor(str, colors);

function printLetterWithColor(str, colors) {
  var div = document.getElementById('test');
  div.insertAdjacentHTML('beforeend', createSpanElementsList(str, colors));
}

function createSpanElementsList(str, colors) {
  var i = 0;
  var colorsLength = colors.length - 1;
  var SpanElementsList = [...str].map(e => {
    if (i > colorsLength) i = 0;
    return createSpanElement(e, i++, colors);
  });

  return SpanElementsList.join('');
}

function createSpanElement(letter, i, colors) {
  var colorStyle = 'color:' + colors[i];
  return letter === ' '
    ? `<span>${letter}</span>`
    : `<span style=${colorStyle}>${letter}</span>`;
}

// https://www.geeksforgeeks.org/find-lost-element-from-a-duplicated-array/

var un = [1, 2, 3, 4, 5, 6];
var deux = [1, 2, 3, 4, 5, 6, 7];
var unLength = un.length;
var deuxLength = deux.length;

function findMissing1(arr1, arr2) {
  var first = arr1.reduce((e, acc) => (acc += e));
  var second = arr2.reduce((e, acc) => (acc += e));

  return first > second ? first - second : second - first;
}

findMissing1(un, deux); // O(n) O(1)

// =====================================

function findMissing2(arr1, arr2, M, N) {
  if (N == M - 1) {
    console.log(`Missing Element is ${findMissingUtil(arr1, arr2, M)}`);
  } else if (M == N - 1) {
    console.log(`Missing Element is ${findMissingUtil(arr2, arr1, N)}`);
  } else {
    console.log('Invalid Input');
  }
}

function findMissingUtil(arr1, arr2, N) {
  // special case, for only element which is missing in second array
  if (N == 1) return arr1[0];

  // special case, for first element missing
  if (arr1[0] != arr2[0]) return arr1[0];

  // Initialize current corner points
  var lo = 0;
  var hi = N - 1;

  // loop until lo < hi
  while (lo < hi) {
    var mid = lo + Math.floor((hi - lo) / 2);

    // If element at mid indices are equal then go to right subarray
    if (arr1[mid] == arr2[mid]) lo = mid;
    else hi = mid;

    // if lo, hi becomes contiguous, break
    if (lo == hi - 1) break;
  }

  // missing element will be at hi index of bigger array
  return arr1[hi];
}

findMissing2(un, deux, unLength, deuxLength); // O(Log n) O(1)

// =====================================

function findMissing3(arr1, arr2, M, N) {
  if (M != N - 1 && N != M - 1) {
    console.log('Invalid Input');
    return;
  }

  // Do XOR of all element
  var res = 0;
  for (var i = 0; i < M; i++) res = res ^ arr1[i];
  for (var i = 0; i < N; i++) res = res ^ arr2[i];

  console.log('Missing element is ' + res);
}

var un = [1, 3, 2, 4, 6, 5];
var deux = [5, 2, 7, 6, 1, 4, 3];
findMissing3(un, deux, un.length, deux.length); // O(n) O(1)
// sum would cause integer overflow for a large sized array not XOR

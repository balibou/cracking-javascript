/*
Given an unsorted integer array, find the smallest missing positive integer.

Example 1:

Input: [1,2,0]
Output: 3
Example 2:

Input: [3,4,-1,1]
Output: 2
Example 3:

Input: [7,8,9,11,12]
Output: 1
Note:

Your algorithm should run in O(n) time and uses constant extra space.
*/

/**
 * @param {number[]} nums
 * @return {number}
 */
var firstMissingPositive = function(nums) {
  // find max number and index all number
  // go from 1 to max, checking existance

  let map = {};

  let max = 0;
  let min = Infinity;

  for (let n of nums) {
    if (n <= 0) {
      continue;
    }

    map[n] = true;

    if (n > max) {
      max = n;
    }

    if (n < min) {
      min = n;
    }
  }

  if (max === 0) {
    return 1;
  }

  if (min > 1) {
    return 1;
  }

  for (let i = 1; i < max; i++) {
    if (!map[i]) {
      return i;
    }
  }

  return max + 1;
};

var merge = require('./merge');

/* l is for left index and r is right index of the
   sub-array of arr to be sorted */
function mergeSort(arr, l, r) {
  if (l < r) {
    var m = l + Math.floor((r - l) / 2);

    // Sort first and second halves
    mergeSort(arr, l, m);
    mergeSort(arr, m + 1, r);

    merge(arr, l, m, r);
  }

  return arr;
}

var arr = [10, 7, 8, 9, 1, 5];
var firstIndex = 0;
var lastIndex = arr.length - 1;
var res = mergeSort(arr, firstIndex, lastIndex);
console.log(res);

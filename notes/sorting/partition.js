/*
  This function takes last element as pivot, 
  places the pivot element at its correct 
  position in sorted array, and places all 
  smaller (smaller than pivot) to left of 
  pivot and all greater elements to right 
  of pivot
*/
function partition(arr, low, high) {
  var pivot = arr[high];
  var i = low - 1; // index of smaller element
  for (var j = low; j < high; j++) {
    // If current element is smaller than or equal to pivot
    if (arr[j] <= pivot) {
      i++;

      // swap arr[i] and arr[j]
      swap(arr, i, j);
    }
  }

  // swap arr[i+1] and arr[high] (or pivot)
  swap(arr, i + 1, high);

  return i + 1;
}

function swap(arr, i, j) {
  var temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

module.exports = partition;

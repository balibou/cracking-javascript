var partition = require('./partition');
// https://www.geeksforgeeks.org/quicksort-better-mergesort/
// https://www.geeksforgeeks.org/why-quick-sort-preferred-for-arrays-and-merge-sort-for-linked-lists/

/**
 * @param {number[]} arr
 * @param {number} low - first index
 * @param {number} high - last index
 * @return {number[]}
 */
function quickSortRecursive(arr, low, high) {
  if (low < high) {
    // pi is partitioning index, arr[pi] is now at right place
    var pi = partition(arr, low, high);

    // Recursively sort elements before partition and after partition
    quickSortRecursive(arr, low, pi - 1);
    quickSortRecursive(arr, pi + 1, high);
  }
  return arr;
}

var arr = [10, 7, 8, 9, 1, 5];
var firstIndex = 0;
var lastIndex = arr.length - 1;
var res = quickSortRecursive(arr, firstIndex, lastIndex);
console.log(res);

var merge = require('./merge');

/* Iterative mergesort function to sort arr[0...n-1] */
function mergeSort(arr, n) {
  var curr_size; // For current size of subarrays to be merged
  // curr_size varies from 1 to n/2
  var left_start; // For picking starting index of left subarray
  // to be merged

  // Merge subarrays in bottom up manner.  First merge subarrays of
  // size 1 to create sorted subarrays of size 2, then merge subarrays
  // of size 2 to create sorted subarrays of size 4, and so on.
  for (curr_size = 1; curr_size <= n - 1; curr_size = 2 * curr_size) {
    // Pick starting point of different subarrays of current size
    for (left_start = 0; left_start < n - 1; left_start += 2 * curr_size) {
      // Find ending point of left subarray. mid+1 is starting
      // point of right
      var mid = left_start + curr_size - 1;

      var right_end = Math.min(left_start + 2 * curr_size - 1, n - 1);

      // Merge Subarrays arr[left_start...mid] & arr[mid+1...right_end]
      merge(arr, left_start, mid, right_end);
    }
  }

  return arr;
}

var arr = [10, 7, 8, 9, 1, 5];
var arrLength = arr.length;
var res = mergeSort(arr, arrLength);
console.log(res);

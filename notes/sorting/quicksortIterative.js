var partition = require('./partition');

/**
 * @param {number[]} arr
 * @param {number} l - first index
 * @param {number} h - last index
 * @return {number[]}
 */
function quickSortIterative(arr, l, h) {
  // Create an auxiliary stack
  var stack = new Array(h - l + 1);

  // initialize top of stack
  var top = -1;

  // push initial values of l and h to stack
  stack[++top] = l;
  stack[++top] = h;

  // Keep popping from stack while is not empty
  while (top >= 0) {
    // Pop h and l
    h = stack[top--];
    l = stack[top--];

    // Set pivot element at its correct position
    // in sorted array
    var p = partition(arr, l, h);

    // If there are elements on left side of pivot,
    // then push left side to stack
    if (p - 1 > l) {
      stack[++top] = l;
      stack[++top] = p - 1;
    }

    // If there are elements on right side of pivot,
    // then push right side to stack
    if (p + 1 < h) {
      stack[++top] = p + 1;
      stack[++top] = h;
    }
  }

  return arr;
}

var arr = [10, 7, 8, 9, 1, 5];
var firstIndex = 0;
var lastIndex = arr.length - 1;
var res = quickSortIterative(arr, firstIndex, lastIndex);
console.log(res);

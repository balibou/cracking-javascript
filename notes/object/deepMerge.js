function deepMerge(obj1, obj2) {
  Object.keys(obj2).forEach(key => {
    if (typeof obj2[key] === 'object') {
      if (!obj1[key]) obj1[key] = {};
      deepMerge(obj1[key], obj2[key]);
    } else {
      obj1[key] = obj2[key];
    }
  });

  return obj1;
}

var obj1 = {
  first: 'oula',
  yoyo: 'yoyo',
  hello: {
    str: 'world',
    str1: 'str1',
    test: {
      fifou: 'fifou'
    }
  }
};

var obj2 = {
  yoyo: 'yaya',
  hello: {
    str1: 'hello',
    test: {
      fifa: 'fifa'
    }
  },
  titi: [1, 2]
};

console.log(mergeDeep(obj1, obj2));

// remove special characters in string except space

var test = 't,e?/s+t';
test.replace(/[^\w\s]/gi, ''); // 'test'

// remove special characters in string with space

var test = 't,e?/s+t';
test.replace(/[^\w]/gi, ''); // 'test'

// formatting a number with exactly two decimals

var test = 10.8;
test.toFixed(2); // "10.80"

// access the first property of an object

var obj = { first: 'someVal' };
obj[Object.keys(obj)[0]]; // 'someVal'

// find middle index of an array

var low = 0;
var high = arr.length - 1;
var mid = low + Math.floor((high - low) / 2);

// return an array of ints
var num = 123456789;
num
  .toString(10)
  .split('')
  .map(t => parseInt(t));

// operators
var i = 0;
console.log(++i); // 1

var j = 0;
console.log(j++); // 0

// add color to html element
<span style="color:blue">blue</span>;

// <div id="one">one</div>
var d1 = document.getElementById('one');
d1.insertAdjacentHTML('afterend', '<div id="two">two</div>');
// At this point, the new structure is:
// <div id="one">one</div><div id="two">two</div>

a = b = c;
// b = c
// a = b

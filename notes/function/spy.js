// https://gist.github.com/cuipengfei/7969634

function Spy(target, method) {
  var originalFunction = target[method];

  // use an object so we can pass by reference, not value
  // i.e. we can return result, but update count from this scope
  var result = {
    count: 0,
    res: null
  };

  // replace method with spy method
  target[method] = function() {
    result.count++;
    result.res = originalFunction.apply(target, arguments);

    return result.res;
  };

  return result;
}

let sample = {
  fn: num => num
};

var spy = Spy(sample, 'fn');
sample.fn();
sample.fn();
console.log('count ', spy.count);
sample.fn();
sample.fn(123);
console.log('count', spy.count);
console.log('res', spy.res);

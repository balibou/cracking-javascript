/*
Write a function that accepts a callback as an argument and returns a SPY. A spy is a shallow clone of the callback that tracks the total number of calls as well as a log of all arguments.

Ex:
const testFunc = (a, b, c) => (a + c) - b;
const spy = iSpy(testFunc);
spy(1, 3, 5) // => 3
spy(6, 8, 9) // => 7
spy.calls() // => 2
spy.args() // => [[1, 3, 5], [6, 8, 9]]
*/

const iSpy = function(func) {
  let counter = 0;
  let arr = [];

  const spy = (...args) => {
    counter++;
    arr.push(args);
    return func(...args);
  };

  spy.calls = () => {
    return counter;
  };

  spy.args = () => {
    return arr;
  };

  return spy;
};

const testFunc = (a, b, c) => a + c - b;
const spy = iSpy(testFunc);
spy(1, 3, 5);
spy(6, 8, 9);
console.log(spy.calls()); // 2
console.log(spy.args()); // [[1, 3, 5], [6, 8, 9]]

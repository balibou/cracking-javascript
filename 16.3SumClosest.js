/*
Given an array nums of n integers and an integer target, find three integers in nums such that the sum is closest to target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

Example:

Given array nums = [-1, 2, 1, -4], and target = 1.

The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
*/

/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var threeSumClosest = function(nums, target) {
  var res = 0;
  if (nums.length < 3) {
    for (var i = 0; i < nums.length; i++) {
      res += nums[i];
      return res;
    }
  }
  nums = nums.sort(function(a, b) {
    return a - b;
  });
  var res = nums[0] + nums[1] + nums[2];
  for (var i = 0; i < nums.length - 2; i++) {
    for (var j = i + 1, k = nums.length - 1; j < k; ) {
      var sum = nums[i] + nums[j] + nums[k];
      if (Math.abs(target - res) >= Math.abs(target - sum)) {
        res = sum;
        if (res == target) return res;
      }
      if (nums[i] + nums[j] + nums[k] > target) {
        k--;
      } else {
        j++;
      }
    }
  }
  return res;
};

/*
Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

For example, given n = 3, a solution set is:

[
  "((()))",
  "(()())",
  "(())()",
  "()(())",
  "()()()"
]
*/

/**
 * @param {number} n
 * @return {string[]}
 */
var generateParenthesis = function(n) {
    var list = [];
    backtrack(list, '', 0, 0, n);
    return list;
  };
  
  function backtrack(ans, cur, open, close, max) {
    if (cur.length === max * 2) {
              ans.push(cur);
              return;
          }
  
          if (open < max) backtrack(ans, cur+"(", open+1, close, max);
          if (close < open) backtrack(ans, cur+")", open, close+1, max);
  }
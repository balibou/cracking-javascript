/*
A linked list is given such that each node
contains an additional random pointer which could
point to any node in the list or null.

Return a deep copy of the list.
*/

/**
 * Definition for singly-linked list with a random pointer.
 * function RandomListNode(label) {
 *     this.label = label;
 *     this.next = this.random = null;
 * }
 */

/**
 * @param {RandomListNode} head
 * @return {RandomListNode}
 */

var copyRandomList = function copyRandomList(head) {
  if (head === null) return head;
  var copies = {};
  var node = head;
  while (node !== null) {
    if (!copies[node.label]) {
      var newCopy = new RandomListNode(node.label);
      copies[node.label] = newCopy;
    }
    node = node.next;
  }

  node = head;
  while (node !== null) {
    var copy = copies[node.label];
    copy.next = node.next ? copies[node.next.label] : null;
    copy.random = node.random ? copies[node.random.label] : null;
    copies[node.label] = copy;
    node = node.next;
  }
  return copies[head.label];
};

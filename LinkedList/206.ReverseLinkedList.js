/*
Reverse a singly linked list.

Example:

Input: 1->2->3->4->5->NULL
Output: 5->4->3->2->1->NULL
Follow up:

A linked list can be reversed either iteratively or recursively.
Could you implement both?
*/

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

// https://leetcode.com/articles/reverse-linked-list/

/*
Approach #1(Iterative)

Time complexity : O(n).
Space complexity : O(1).
*/

var reverseList = function(head) {
  var prev = null;
  var curr = head;
  while (curr != null) {
    var nextTemp = curr.next;
    curr.next = prev;
    prev = curr;
    curr = nextTemp;
  }
  return prev;
};

/*
Approach #2 (Recursive)

Time complexity : O(n).
Space complexity : O(n).
*/

var reverseList2 = function(head) {
  if (head == null || head.next == null) return head;
  var p = reverseList2(head.next);
  head.next.next = head;
  head.next = null;
  return p;
};

// Given a linked list, determine if it has a cycle in it.

// To represent a cycle in the given linked list, we use an integer pos which represents the position (0-indexed) in the linked list where tail connects to. If pos is -1, then there is no cycle in the linked list.

// Input: head = [3,2,0,-4], pos = 1
// Output: true
// Explanation: There is a cycle in the linked list, where tail connects to the second node.

/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */

/*
Approach 1: Hash Table

Time complexity : O(n).
Space complexity : O(n).
*/

var hasCycle = function hasCycle(head) {
  var nodes = new Set();
  while (head) {
    if (nodes.has(head)) return true;
    nodes.add(head);
    head = head.next;
  }
  return false;
};

/*
Approach 2: Two Pointers

Time complexity : O(n).
Space complexity : O(1).
*/

var hasCycle = function(head) {
  if (head == null || head.next == null) {
    return false;
  }
  var slow = head;
  var fast = head.next;
  while (slow != fast) {
    if (fast == null || fast.next == null) {
      return false;
    }
    slow = slow.next;
    fast = fast.next.next;
  }
  return true;
};

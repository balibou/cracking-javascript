/*

Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3 
Explanation: The answer is "abc", with the length of 3. 
Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3. 
             Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
*/

/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
  var hash = {};
  var n = s.length;
  var i = 0;
  var j = 0;
  var ans = 0;
  while (i < n && j < n) {
    if (hash[s[j]] === undefined) {
      hash[s[j++]] = true;
      if (j - i > ans) ans = j - i;
    } else {
      delete hash[s[i++]];
    }
  }
  return ans;
};

/*
Time complexity : O(2n)=O(n).
In the worst case each character will be visited twice by i and j.

Space complexity : O(min(m,n)).
Same as the previous approach.
We need O(k) space for the sliding window, where k is the size of the Set.
The size of the Set is upper bounded by the size of the string n and the size of the charset/alphabet m. 
*/
